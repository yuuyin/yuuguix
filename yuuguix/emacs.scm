(define-module (yuuguix emacs))

(define emacs-master-native-comp
  (package
   (inherit emacs-native-comp)
   #:pkg-name "emacs-master-native-comp"
   #:pkg-version "unstable-2022-08-03"
   #:pkg-revision "158691"
   #:git-repo "https://git.savannah.gnu.org/git/emacs.git"
   ;; #:git-repo "https://github.com/emacs-mirror/emacs.git"
   #:git-commit "cfb295f1e55e4d04beaad5d57ede494c436cf277"
   #:checksum ""))
