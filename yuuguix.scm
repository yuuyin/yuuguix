;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LOAD
(let ((yuuguix-path-root (dirname (current-filename))))
  (add-to-load-path yuuguix-path-root)
  (add-to-load-path (string-append yuuguix-path-root "/" "yuuguix"))
  %load-path)
;; load yuuguix defined channels file.
(load-from-path "channel")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MODULE :: DEFINE / USE
(define-module (yuuguix)
  #:use-module (gnu bootloader)		      ; for bootloader-configuration
  #:use-module (gnu bootloader grub)	      ; for grub
  #:use-module (gnu packages)		      ; for packages
  #:use-module (gnu packages base)	      ; for coretuils
  #:use-module (gnu packages admin)	      ; for tree
  #:use-module (gnu packages curl)	      ; for curl, curlie
  #:use-module (gnu packages emacs)	      ; for emacs-next
  #:use-module (gnu packages file)	      ; for file
  #:use-module (gnu packages rust-apps)	      ; for fd, ripgrep
  #:use-module (gnu packages shells)	      ; for mksh, bash, nushell
  #:use-module (gnu packages ssh)	      ; for openssh
  #:use-module (gnu packages tmux)	      ; for tmux
  #:use-module (gnu packages version-control) ; for git
  #:use-module (gnu services)		      ; for service
  #:use-module (gnu services base)	      ; for %base-services
  #:use-module (gnu services networking)      ; for dhcp-client-service-type
  #:use-module (gnu services ssh)	      ; for openssh
  #:use-module (gnu system)		      ; for operating-system, %base-packages
  #:use-module (gnu system file-systems)      ; for file-system
  #:use-module (gnu system keyboard)	      ; for keyboard-layout
  #:use-module (gnu system shadow)	      ; for user-account
  #:use-module (guix gexp)		      ; for file-append
  #:use-module (oop goops)		      ; for class-of
  #:use-module (yuuguix channel))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CHANNEL
(yuuguix-channel)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PACKAGES

(define yuuguix-shell-packages (list mksh
				 ;; nushell
				 ))

(define yuuguix-network-packages (list curl
				   curlie))

(define yuuguix-configuration-management-packages (list git))

(define yuuguix-emacs-packages (list emacs-next))

(define yuuguix-editor-packages (append ;; (list helix)
				 yuuguix-emacs-packages))

(define yuuguix-system-file-packages (list coreutils
				       fd
				       file
				       ripgrep
				       tree))

(define yuuguix-system-monitor-packages (list htop))

;; (define yuuguix-system-boot-loader-packages (list efibootmgr))

(define yuuguix-misc-packages (list tmux))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; OPERATING SYSTEM
(operating-system
  (host-name "libvirt-guix")
  (timezone "America/Sao_Paulo")
  (locale "en_DK.utf8")
  (keyboard-layout (keyboard-layout
		    "br" "abnt2"
		    #:options '("ctrl:nocaps")))

  ;; Boot in UEFI mode, assuming
  ;; /dev/disk/by-partuuid/64931280-7a2a-4df5-b650-54e959776f6a
  ;; is the target storage,
  ;; and "" is the PARTUUID of the target root file system.
  (bootloader (bootloader-configuration
	       (bootloader grub-efi-bootloader)
	       (targets '("/efi"))
	       (keyboard-layout keyboard-layout)))

  ;; MOUNT
  (file-systems (let* ((%yuuguix-base-mount-options-btrfs
			'(("compress-force" . "zstd")
			  ("space_cache" . "v2")
			  ("discard" . "async")
			  "ssd"))
		       (%yuuguix-base-mount-flags-no-btrfs
			'(no-atime))
		       (%yuuguix-base-mount-flags-no
			'(no-exec
			  no-dev
			  no-suid))
		       ;; ID 256 gen 146 top level 5 path @os-guix
		       (%file-system-os-guix
			(file-system
			  (device (uuid "64931280-7a2a-4df5-b650-54e959776f6a"))
			  (type "btrfs")
			  (mount-point "/")
			  ;; (needed-for-boot? #t)
			  (flags '(no-atime))
			  (options (alist->file-system-options
				    (append '(("subvolid" . "256"))
					    %yuuguix-base-mount-options-btrfs))))))
		  (append (list
			   %file-system-os-guix

			   (file-system
			     (device (uuid "8638-3115" 'fat))
			     (type "vfat")
			     (mount-point "/efi")
			     ;; (flags %yuuguix-base-mount-flags-no)
			     ;; (options '("esp"))
			     ;; (dependencies (filter (file-system-mount-point-predicate "/")
			     ;;			    file-systems))
			     ;; (dependencies (list %file-system-os-guix))
			     (needed-for-boot? #t))

			   ;; ID 258 gen 10 top level 5 path @os-guix-snapshot-rw
			   (file-system
			     (device (uuid "64931280-7a2a-4df5-b650-54e959776f6a"))
			     (type "btrfs")
			     (mount-point "/snapshot")
			     (flags (append %yuuguix-base-mount-flags-no
					    %yuuguix-base-mount-flags-no-btrfs))
			     (options (alist->file-system-options
				       (append '(("subvolid" . "258"))
					       %yuuguix-base-mount-options-btrfs)))
			     ;; (dependencies (list %file-system-os-guix))
			     )

			   ;; ID 259 gen 11 top level 5 path @os-guix-persist
			   (file-system
			     (device (uuid "64931280-7a2a-4df5-b650-54e959776f6a"))
			     (type "btrfs")
			     (mount-point "/persist")
			     (flags %yuuguix-base-mount-flags-no)
			     (options (alist->file-system-options
				       (append '(("subvolid" . "259"))
					       %yuuguix-base-mount-options-btrfs)))
			     ;; (dependencies (list %file-system-os-guix))
			     )

			   ;; ID 260 gen 12 top level 5 path @os-guix-gnu
			   (file-system
			     (device (uuid "64931280-7a2a-4df5-b650-54e959776f6a"))
			     (type "btrfs")
			     (mount-point "/gnu")
			     (options (alist->file-system-options
				       (append '(("subvolid" . "260")
						 ;; needs subvol because in
						 ;; guix/gnu/system/file-system.scm,
						 ;; btrfs-store-subvolume-file-name,
						 ;; raise.
						 ;; XXX: Deriving the subvolume name
						 ;; based from a subvolume ID is not supported,
						 ;; as we'd need to query the actual file system.
						 ("subvol" . "@os-guix-gnu"))
					       %yuuguix-base-mount-options-btrfs)))
			     ;; (dependencies (list %file-system-os-guix))
			     )

			   ;; ID 261 gen 13 top level 5 path @os-guix-home
			   (file-system
			     (device (uuid "64931280-7a2a-4df5-b650-54e959776f6a"))
			     (type "btrfs")
			     (mount-point "/home")
			     (options (alist->file-system-options
				       (append '(("subvolid" . "261"))
					       %yuuguix-base-mount-options-btrfs)))
			     ;; (dependencies (list %file-system-os-guix))
			     )

			   ;; ID 262 gen 14 top level 5 path @os-guix-var-log
			   (file-system
			     (device (uuid "64931280-7a2a-4df5-b650-54e959776f6a"))
			     (type "btrfs")
			     (mount-point "/var/log")
			     (flags %yuuguix-base-mount-flags-no)
			     (options (alist->file-system-options
				       (append '(("subvolid" . "262"))
					       %yuuguix-base-mount-options-btrfs)))
			     ;; (dependencies (list %file-system-os-guix))
			     )

			   ;; ID 263 gen 22 top level 5 path @os-guix-swap
			   (file-system
			     (device (uuid "64931280-7a2a-4df5-b650-54e959776f6a"))
			     (type "btrfs")
			     (mount-point "/swap")
			     (flags %yuuguix-base-mount-flags-no)
			     (options (alist->file-system-options
				       (append '(("subvolid" . "263"))
					       %yuuguix-base-mount-options-btrfs)))
			     ;; (dependencies (list %file-system-os-guix))
			     ))
			  %base-file-systems)))

  ;; Specify a swap file for the system,
  ;; which resides on the root file system.
  (swap-devices (list (swap-space
		       (target "/swap/swapfile")
		       (dependencies (filter (file-system-mount-point-predicate "/swap")
					     file-systems)))))

  ;; Specify user accounts.
  ;; "root" user is implicit, and is initially created with the empty password.
  (users (append (list
		  (user-account
		   (name "yuu")
		   (uid 1024)
		   ;; (password (crypt "InitialPassword!" "$6$abc"))
		   (comment "yλ✨✨✨✨")
		   (create-home-directory? #t)
		   (home-directory "/home/yuu")
		   (group "users")
		   (supplementary-groups
		    '("wheel"    ; sudo (sudoer)
		      "audio"    ; control audio devices
		      "video"))  ; conrtrol video devices (webcam, ...)
		   (shell (file-append
			   ;; nu "/bin/nu"
			   mksh "/bin/mksh"))))
		 %base-user-accounts))

  ;; Globally installed pacakges
  (packages (append yuuguix-configuration-management-packages
		    yuuguix-emacs-packages
		    yuuguix-misc-packages
		    yuuguix-network-packages
		    yuuguix-system-file-packages
		    yuuguix-system-monitor-packages
		    yuuguix-shell-packages
		    %base-packages))

  ;; Add services to the baseline: DHCP, OPENSSH.
  (services (append
	     (list (service dhcp-client-service-type)
		   (service openssh-service-type (openssh-configuration
						  (openssh openssh-sans-x)
						  (port-number 924))))
	     %base-services)))
